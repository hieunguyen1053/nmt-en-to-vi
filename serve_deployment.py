import os
from typing import List

import torch
from transformers import AutoModelForSeq2SeqLM, AutoTokenizer
from pipeline import TranslationPipeline
from ray import serve
from starlette.requests import Request

MODEL_PATH = os.environ.get('MODEL_PATH', None)
HF_TOKEN = os.environ.get('HF_TOKEN', None)

if MODEL_PATH is None or HF_TOKEN is None:
    raise ValueError("Please set MODEL_PATH and HF_TOKEN environment variables.")


@serve.deployment(
    num_replicas=1,
    route_prefix="/translation/inference",
    ray_actor_options={
        "num_cpus": 1,
        "num_gpus": 1,
    }
)
class Translator:
    def __init__(self):
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        tokenizer = AutoTokenizer.from_pretrained(MODEL_PATH, use_auth_token=HF_TOKEN)
        model = AutoModelForSeq2SeqLM.from_pretrained(MODEL_PATH, use_cache=True, use_auth_token=HF_TOKEN)
        self.translator = TranslationPipeline(
            model=model,
            tokenizer=tokenizer,
            device=device,
        )

    @serve.batch(max_batch_size=8)
    async def handle_batch(self, text: List[str], prev_text: List[str], prev_translated_text: List[str]) -> List[str]:
        results = self.translator({'text': text, 'prev_text': prev_text, 'prev_translated_text': prev_translated_text})
        return results

    async def __call__(self, request: Request):
        data = await request.json()

        text = data['text']
        prev_text = data.get('prev_text', None)
        prev_translated_text = data.get('prev_translated_text', None)

        return await self.handle_batch(text, prev_text, prev_translated_text)

Translator.deploy()