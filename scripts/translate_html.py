import requests
from bs4 import BeautifulSoup

BASE_URL = 'http://localhost:8001'

KEEP_ATTR_RULES = {
    'a': ['href'],
    'img': ['src', 'alt'],
}

def strip_attributes(html_string):
    soup = BeautifulSoup(html_string, 'html.parser')
    for tag in soup.find_all(True):
        if tag.name in KEEP_ATTR_RULES:
            tag.attrs = {k:v for k,v in tag.attrs.items() if k in KEEP_ATTR_RULES[tag.name]}
        else:
            tag.attrs = {}

    return str(soup)

def translate_html(html_string, base_url=BASE_URL):
    response = requests.get(f'{base_url}/-/healthz')
    if response.status_code != 200:
        raise Exception('Translation server is not working')

    soup = BeautifulSoup(html_string, 'html.parser')
    prev_text = None
    prev_translated_text = None

    for p in soup.find_all(['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li']):
        inner_html = ''.join(str(x) for x in p.contents)
        if p.get_text(strip=True) == '':
            continue
        text = strip_attributes(inner_html)
        response = requests.post(f'{base_url}/translation/inference', json={'text': text, 'prev_text': prev_text, 'prev_translated_text': prev_translated_text})
        translated_text = response.json()['translated_text']

        prev_text = text
        prev_translated_text = translated_text

        p.clear()
        p.append(BeautifulSoup(translated_text, 'html.parser'))

    translated_html_string = str(soup)
    return translated_html_string