#!/bin/bash

serve run config.yaml &

gunicorn -b 0.0.0.0:8888 "app:app" &

wait