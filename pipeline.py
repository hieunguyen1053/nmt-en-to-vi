import torch
from typing import List, Union
from transformers import Pipeline

class TranslationPipeline(Pipeline):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _sanitize_parameters(self, **kwargs):
        preprocess_params = {}
        forward_params = {}
        postprocess_params = {}
        return preprocess_params, forward_params, postprocess_params

    def adapt_length_inputs(self, input_length: int, min_length: int, max_length: int):
        if input_length > 0.9 * max_length:
            max_length = int(input_length * 2.0)
        return min_length, max_length

    def _parse_and_tokenize(self, args):
        assert 'text' in args, "You need to provide a 'text' key to use this pipeline."
        assert 'prev_text' in args, "You need to provide a 'prev_text' key to use this pipeline."
        assert 'prev_translated_text' in args, "You need to provide a 'prev_translated_text' key to use this pipeline."

        args['prev_text'] = [text if text else "" for text in args['prev_text']]
        args['prev_translated_text'] = [text if text else "<pad>" for text in args['prev_translated_text']]

        if isinstance(args['text'], str):
            inputs = self.tokenizer(args['prev_text'] + args['text'], return_tensors=self.framework)
            decoder_inputs = self.tokenizer(args['prev_translated_text'], add_special_tokens=False, return_tensors=self.framework)
            inputs['decoder_input_ids'] = decoder_inputs['input_ids']
            inputs['prev_translated_text'] = [args['prev_translated_text']]
        elif isinstance(args['text'], list):
            inputs = self.tokenizer([prev_text + text for prev_text, text in zip(args['prev_text'], args['text'])], return_tensors=self.framework,  padding=True)
            decoder_inputs = self.tokenizer(args['prev_translated_text'], add_special_tokens=False, return_tensors=self.framework, padding=True)
            inputs['decoder_input_ids'] = decoder_inputs['input_ids']
            inputs['prev_translated_text'] = args['prev_translated_text']
        return inputs

    def __call__(self, texts: Union[str, List[str]], **kwargs):
        return super().__call__(texts,  **kwargs)

    def preprocess(self, inputs):
        inputs = self._parse_and_tokenize(inputs)
        return inputs

    def _forward(self, model_inputs, **generate_kwargs):
        _, input_length = model_inputs["input_ids"].shape

        prev_translated_text = model_inputs.get("prev_translated_text", None)
        if "prev_translated_text" in model_inputs:
            del model_inputs["prev_translated_text"]

        generate_kwargs["min_length"] = generate_kwargs.get("min_length", self.model.config.min_length)
        generate_kwargs["max_length"] = generate_kwargs.get("max_length", self.model.config.max_length)
        generate_kwargs["min_length"], generate_kwargs["max_length"] = self.adapt_length_inputs(input_length, generate_kwargs["min_length"], generate_kwargs["max_length"])

        output_ids = self.model.generate(**model_inputs, **generate_kwargs)
        return {'output_ids': output_ids, 'prev_translated_text': prev_translated_text}

    def postprocess(self, model_outputs):
        outputs = self.tokenizer.batch_decode(model_outputs['output_ids'], skip_special_tokens=True)
        outputs = [text0.replace(text1, '').strip() for text0, text1 in zip(outputs, model_outputs['prev_translated_text'])]
        outputs = [{'translated_text': text} for text in outputs]
        if torch.cuda.is_available():
            torch.cuda.empty_cache()
            torch.cuda.ipc_collect()
        return outputs