import json
import html
from concurrent.futures import ThreadPoolExecutor, as_completed

from flask import Flask, Response, request
from flask_cors import CORS

import requests
from bs4 import BeautifulSoup


app = Flask(__name__)
CORS(app)

BASE_URL = 'http://localhost:8000' # DEFAULT PORT FOR RAY SERVE

KEEP_ATTR_RULES = {
    'a': ['href'],
    'img': ['src', 'alt'],
}

def strip_attributes(html_string, return_soup=False):
    soup = BeautifulSoup(html_string, 'html.parser')
    for tag in soup.find_all(True):
        if tag.name in KEEP_ATTR_RULES:
            tag.attrs = {k:v for k,v in tag.attrs.items() if k in KEEP_ATTR_RULES[tag.name]}
        else:
            tag.attrs = {}
    if return_soup:
        return soup
    else:
        return str(soup)


@app.route("/v1/translate", methods=["POST"])
def translate():
    data = request.get_json()

    if 'text' not in data:
        return Response(response=json.dumps({'error': 'text is required'}, ensure_ascii=False), status=400, mimetype='application/json')

    text = data['text']
    prev_text = data.get('prev_text', '')
    prev_translated_text = data.get('prev_translated_text', '')

    resp = requests.post(f'{BASE_URL}/translation/inference', json={
        'text': text,
        'prev_text': prev_text + '<br>',
        'prev_translated_text': prev_translated_text + '<br>'
    })
    if resp.status_code != 200:
        return Response(response=json.dumps({'error': 'Translation server is not working'}, ensure_ascii=False), status=500, mimetype='application/json')

    translated_text = resp.json()['translated_text']
    return Response(response=json.dumps({'translated_text': translated_text}, ensure_ascii=False), status=200, mimetype='application/json')


@app.route("/v1/translate_html", methods=["POST"])
def translate_html():
    data = request.get_json()

    if 'html' not in data:
        return Response(response=json.dumps({'error': 'html is required'}, ensure_ascii=False), status=400, mimetype='application/json')

    is_strip_attributes = data.get('is_strip_attributes', False)

    html_string = html.unescape(data['html'])
    soup = BeautifulSoup(html_string, 'html.parser')
    prev_text = None
    prev_translated_text = None

    if is_strip_attributes:
        soup = strip_attributes(html_string, return_soup=True)

    tags = []
    texts = []
    translated_texts = []
    for p in soup.find_all(['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li']):
        inner_html = ''.join(str(x) for x in p.contents)
        # Skip tags with no text
        if p.get_text(strip=True) == '':
            continue

        # Strip attributes
        text = strip_attributes(inner_html)

        tags.append(p)
        texts.append(text)

    # Translate first sentence to get context

    response = requests.post(f'{BASE_URL}/translation/inference', json={
        'text': texts[0],
        'prev_text': prev_text,
        'prev_translated_text': prev_translated_text
    })

    translated_text = response.json()['translated_text']
    prev_text = texts[0]
    prev_translated_text = translated_text

    translated_texts.append(translated_text)

    with ThreadPoolExecutor(max_workers=8) as executor:
        futures = []
        future2idx = {}

        for i in range(1, len(texts)):
            futures.append(executor.submit(requests.post, f'{BASE_URL}/translation/inference', json={
                'text': texts[i],
                'prev_text': prev_text + '<br>',
                'prev_translated_text': prev_translated_text + '<br>'
            }))
            future2idx[futures[-1]] = i

        idx2result = {}
        for result in as_completed(futures):
            idx = future2idx[result]
            idx2result[idx] = result.result()

    for i in range(1, len(texts)):
        translated_text = idx2result[i].json()['translated_text']
        translated_texts.append(translated_text)

    for i in range(len(tags)):
        tags[i].clear()
        tags[i].append(BeautifulSoup(translated_texts[i], 'html.parser'))

    translated_html_string = str(soup)
    return Response(response=json.dumps({'translated_html': translated_html_string}, ensure_ascii=False), status=200, mimetype='application/json')


@app.before_request
def before():
    secret_key = request.headers.get('Authorization', '')

    if request.path == '/ping':
        return

    if secret_key != "8#eW2!zb5hLiVc$":
        return 'Invalid secret key', 401


@app.route("/ping")
def ping():
    return "pong"