FROM python:3.9
WORKDIR /ray-serve/nmt
COPY ./requirements.txt /ray-serve/nmt/requirements.txt
RUN pip install -r requirements.txt

COPY ./config.yaml /ray-serve/nmt/config.yaml
COPY ./pipeline.py /ray-serve/nmt/pipeline.py
COPY ./serve_deployment.py /ray-serve/nmt/serve_deployment.py
COPY ./app.py /ray-serve/nmt/app.py

COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

EXPOSE 8888

ENV RAY_memory_monitor_refresh_ms=0
ENV OMP_NUM_THREADS=1

ENTRYPOINT ["/entrypoint.sh"]